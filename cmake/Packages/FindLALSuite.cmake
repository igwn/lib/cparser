# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

# Load using standard package finder
find_package_standard(
  LALSuite
  NAMES lal lalinspiral lalmetaio lalburst lalpulsar lalframe lalsimulation lalinference lalsupport
  HEADERS "lal"
  PATHS $ENV{LALSUITE} ${LALSUITE} $ENV{LALSuite} ${LALSuite}
)

get_filename_component(FILE_NAME ${CMAKE_CURRENT_LIST_FILE} NAME_WE)
if(${FILE_NAME} MATCHES "^Find(.+)$")

    set(FINDER ${CMAKE_MATCH_1})
    if(${FINDER}_FOUND)
      find_package(GSL REQUIRED)
    endif()
endif()