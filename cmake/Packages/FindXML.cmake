# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

# Load using standard package finder
find_package_standard(
  NAMES xml2
  HEADERS "libxml/xmlIO.h"
  PATHS ${LIBXML2} ${LIBXML} $ENV{LIBXML2} $ENV{LIBXML}
)

