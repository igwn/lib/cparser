#pragma once

#include <RCParser/TConfigAdapter.h>
#include <TString.h>

#ifdef JSON_FOUND

// Concrete adapter class for JSON format
class TConfigJson : public TConfigAdapter {

    public:
    
        TConfigNode *Parse(const char *contents, int size) override;

        const char *GetName() override { return "json"; }
        void Save(const char *filename, const TConfigNode& root) override;
        bool Supports(const char *filename) override;
};

#endif
