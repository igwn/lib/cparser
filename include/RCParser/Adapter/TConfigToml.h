#pragma once

#include <RCParser/TConfigAdapter.h>
#include <TString.h>

#ifdef TOML_FOUND

// Concrete adapter class for TOML format
class TConfigToml : public TConfigAdapter {

    public:

        TConfigNode *Parse(const char *contents, int size) override;

        const char *GetName() override { return "toml"; }
        void Save(const char *filename, const TConfigNode& root) override;
        bool Supports(const char *filename) override;
};

#endif
