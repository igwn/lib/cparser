#pragma once 

#include <RCParser/TConfigAdapter.h>
#include <TString.h>

#ifdef XML_FOUND

// Concrete adapter class for XML format
class TConfigXml : public TConfigAdapter {

    public:

        TConfigNode *Parse(const char *contents, int size) override;

        const char *GetName() override { return "xml"; }
        void Save(const char *filename, const TConfigNode& root) override;
        bool Supports(const char *filename) override;
};

#endif
