#pragma once 

#include <Rioplus.h>
#include <TH1.h>
#include <vector>

#include <TClock.h>
#include <TCli.h>

#include <lal/LALStdlib.h>
#include <lal/FrequencySeries.h>
#include <lal/TimeSeries.h>
#include <lal/Units.h>
#include <lal/LALSimNoise.h>
#include <lal/Date.h>

namespace LALSuite {

    //
    // LAL struct to time vectors
    inline std::vector<double> __vector(REAL8TimeSeries *series) 
    {
        int length = series->data->length;
        std::vector<double> data(length);

        for(int i = 0; i < length; i++)
            data[i] = series->data->data[i];

        return data;
    }
    inline std::vector<float> __vector(REAL4TimeSeries *series) 
    {
        int length = series->data->length;
        std::vector<float> data(length);

        for(int i = 0; i < length; i++)
            data[i] = series->data->data[i];

        return data;
    }

    inline std::vector<std::complex<double>> __vector(COMPLEX16TimeSeries *series) 
    {
        int length = series->data->length;
        std::vector<std::complex<double>> data(length);

        for(int i = 0; i < length; i++)
            data[i] = std::complex<double>(creal(series->data->data[i]), cimag(series->data->data[i]));

        return data;
    }

    inline std::vector<std::complex<float>> __vector(COMPLEX8TimeSeries *series) 
    {
        int length = series->data->length;
        std::vector<std::complex<float>> data(length);

        for(int i = 0; i < length; i++)
            data[i] = std::complex<double>(creal(series->data->data[i]), cimag(series->data->data[i]));

        return data;
    }

    inline std::vector<std::complex<double>> __vector(REAL8TimeSeries *realSeries, REAL8TimeSeries *imagSeries) 
    {
        int length = TMath::Max(realSeries->data->length, imagSeries->data->length);
        std::vector<std::complex<double>> data(length);

        for(int i = 0; i < length; i++) {

            data[i] = std::complex<double>(
                realSeries->data->length > i ? realSeries->data->data[i] : 0, 
                imagSeries->data->length > i ? imagSeries->data->data[i] : 0
            );
        }

        return data;
    }

    inline std::vector<std::complex<float>> __vector(REAL4TimeSeries *realSeries, REAL4TimeSeries *imagSeries) 
    {
        int length = TMath::Max(realSeries->data->length, imagSeries->data->length);
        std::vector<std::complex<float>> data(length);

        for(int i = 0; i < length; i++) {

            data[i] = std::complex<double>(
                realSeries->data->length > i ? realSeries->data->data[i] : 0, 
                imagSeries->data->length > i ? imagSeries->data->data[i] : 0
            );
        }

        return data;
    }


    //
    // LAL struct to frequency vectors
    inline std::vector<double> __vector(REAL8FrequencySeries *series) 
    {
        int length = series->data->length;
        std::vector<double> data(length);

        for(int i = 0; i < length; i++)
            data[i] = series->data->data[i];

        return data;
    }

    inline std::vector<float> __vector(REAL4FrequencySeries *series) 
    {
        int length = series->data->length;
        std::vector<float> data(length);

        for(int i = 0; i < length; i++)
            data[i] = series->data->data[i];

        return data;
    }

    inline std::vector<std::complex<double>> __vector(COMPLEX16FrequencySeries *series) 
    {
        int length = series->data->length;
        std::vector<std::complex<double>> data(length);

        for(int i = 0; i < length; i++)
            data[i] = std::complex<double>(creal(series->data->data[i]), cimag(series->data->data[i]));

        return data;
    }
    inline std::vector<std::complex<float>> __vector(COMPLEX8FrequencySeries *series) 
    {
        int length = series->data->length;
        std::vector<std::complex<float>> data(length);

        for(int i = 0; i < length; i++)
            data[i] = std::complex<double>(creal(series->data->data[i]), cimag(series->data->data[i]));

        return data;
    }

    inline std::vector<std::complex<double>> __vector(REAL8FrequencySeries *realSeries, REAL8FrequencySeries *imagSeries) 
    {
        int length = TMath::Max(realSeries->data->length, imagSeries->data->length);

        std::vector<std::complex<double>> data(length);
        for(int i = 0; i < length; i++) {

            data[i] = std::complex<double>(
                realSeries->data->length > i ? realSeries->data->data[i] : 0, 
                imagSeries->data->length > i ? imagSeries->data->data[i] : 0
            );
        }

        return data;
    }

    inline std::vector<std::complex<float>> __vector(REAL4FrequencySeries *realSeries, REAL4FrequencySeries *imagSeries) 
    {
        int length = TMath::Max(realSeries->data->length, imagSeries->data->length);

        std::vector<std::complex<float>> data(length);
        for(int i = 0; i < length; i++) {

            data[i] = std::complex<double>(
                realSeries->data->length > i ? realSeries->data->data[i] : 0, 
                imagSeries->data->length > i ? imagSeries->data->data[i] : 0
            );
        }

        return data;
    }

    //
    // ROOT histogram to LAL struct methods
    inline REAL8TimeSeries* __REAL8TimeSeries(TH1D *h) 
    {
        if(h == NULL) return NULL;
        
        REAL8TimeSeries *series = new REAL8TimeSeries;
        for(int i = 0, N = LALNameLength; i < TMath::Min(TString(h->GetName()).Length(), N); i++)
            series->name[i] = h->GetName()[i];

        double GPS = h->GetXaxis()->GetXmin();
        unsigned int GTimeS = (unsigned int) GPS;
        unsigned int GTimeN = 1e9 * (GPS - GTimeS);
        series->epoch = {static_cast<INT4>(GTimeS), static_cast<INT4>(GTimeN)};
        series->f0 = 0;
        series->deltaT = h->GetBinWidth(1);
        series->sampleUnits = lalStrainUnit;

        series->data = new REAL8Sequence;
        series->data->length = h->GetNbinsX();
        series->data->data = new REAL8[series->data->length];
        for(int i = 0; i < series->data->length; i++)
            series->data->data[i] = h->GetBinContent(i+1);

        return series;
    }

    inline REAL4TimeSeries* __REAL4TimeSeries(TH1F *h) 
    {
        if(h == NULL) return NULL;

        REAL4TimeSeries *series = new REAL4TimeSeries;
        for(int i = 0, N = LALNameLength; i < TMath::Min(TString(h->GetName()).Length(), N); i++)
            series->name[i] = h->GetName()[i];

        double GPS = h->GetXaxis()->GetXmin();
        unsigned int GTimeS = (unsigned int) GPS;
        unsigned int GTimeN = 1e9 * (GPS - GTimeS);
        series->epoch = {static_cast<INT4>(GTimeS), static_cast<INT4>(GTimeN)};
        series->f0 = 0;
        series->deltaT = h->GetBinWidth(1);
        series->sampleUnits = lalStrainUnit;
        
        series->data = new REAL4Sequence;
        series->data->length = h->GetNbinsX();
        series->data->data = new REAL4[series->data->length];        
        for(int i = 0; i < series->data->length; i++)
            series->data->data[i] = h->GetBinContent(i+1);        

        return series;
    }

    inline REAL8FrequencySeries* __REAL8FrequencySeries(TH1D *h) 
    {
        if(h == NULL) return NULL;

        REAL8FrequencySeries *series = new REAL8FrequencySeries;
        for(int i = 0, N = LALNameLength; i < TMath::Min(TString(h->GetName()).Length(), N); i++)
            series->name[i] = h->GetName()[i];

        std::vector<TString> metadata = ROOT::IOPlus::Helpers::Explode(' ', h->GetTitle());
        double GPS = metadata.size() > 1 ? metadata[1].Atof() : 0;
        unsigned int GTimeS = (unsigned int) GPS;
        unsigned int GTimeN = 1e9 * (GPS - GTimeS);
        
        series->epoch = {static_cast<INT4>(GTimeS), static_cast<INT4>(GTimeN)};
        series->f0 = 0;
        series->deltaF = h->GetBinWidth(1);
        series->sampleUnits = lalSecondUnit;
        
        series->data = new REAL8Sequence;
        series->data->length = h->GetNbinsX() % 2 != 0 ? h->GetNbinsX()/2 + 1 : h->GetNbinsX();
        series->data->data = new REAL8[series->data->length];        
        for(int i = 0; i < series->data->length; i++)
            series->data->data[i] = h->GetBinContent(i+1);        

        return series;
    }

    inline REAL4FrequencySeries* __REAL4FrequencySeries(TH1F *h) 
    {
        if(h == NULL) return NULL;

        REAL4FrequencySeries *series = new REAL4FrequencySeries;
        for(int i = 0, N = LALNameLength; i < TMath::Min(TString(h->GetName()).Length(), N); i++)
            series->name[i] = h->GetName()[i];

        std::vector<TString> metadata = ROOT::IOPlus::Helpers::Explode(' ', h->GetTitle());
        double GPS = metadata.size() > 1 ? metadata[1].Atof() : 0;
        unsigned int GTimeS = (unsigned int) GPS;
        unsigned int GTimeN = 1e9 * (GPS - GTimeS);
        
        series->epoch = {static_cast<INT4>(GTimeS), static_cast<INT4>(GTimeN)};
        series->f0 = 0;
        series->deltaF = h->GetBinWidth(1);
        series->sampleUnits = lalSecondUnit;

        series->data = new REAL4Sequence;        
        series->data->length = h->GetNbinsX() % 2 != 0 ? h->GetNbinsX()/2 + 1 : h->GetNbinsX();
        series->data->data = new REAL4[series->data->length];        
        for(int i = 0; i < series->data->length; i++)
            series->data->data[i] = h->GetBinContent(i+1);        

        return series;
    }

    inline REAL8TimeSeries* __REAL8TimeSeries(TString name, std::vector<double> &y, double dt, double GPS = 0) 
    {
        REAL8TimeSeries *series = new REAL8TimeSeries;
        for(int i = 0, N = LALNameLength; i < TMath::Min(TString(name).Length(), N); i++)
            series->name[i] = name[i];

        unsigned int GTimeS = (unsigned int) GPS;
        unsigned int GTimeN = 1e9 * (GPS - GTimeS);
        series->epoch = {static_cast<INT4>(GTimeS), static_cast<INT4>(GTimeN)};
        series->f0 = 0;
        series->deltaT = dt;
        series->sampleUnits = lalStrainUnit;

        series->data = new REAL8Sequence;
        series->data->length = y.size();
        series->data->data = new REAL8[series->data->length];      
        for(int i = 0; i < series->data->length; i++)
            series->data->data[i] = y[i];

        return series;
    }
    inline REAL4TimeSeries* __REAL4TimeSeries(TString name, std::vector<float> &y, double dt, double GPS = 0) 
    {
        REAL4TimeSeries *series = new REAL4TimeSeries;

        for(int i = 0, N = LALNameLength; i < TMath::Min(TString(name).Length(), N); i++)
            series->name[i] = name[i];

        unsigned int GTimeS = (unsigned int) GPS;
        unsigned int GTimeN = 1e9 * (GPS - GTimeS);
        series->epoch = {static_cast<INT4>(GTimeS), static_cast<INT4>(GTimeN)};
        series->f0 = 0;
        series->deltaT = dt;
        series->sampleUnits = lalStrainUnit;

        series->data = new REAL4Sequence;
        series->data->length = y.size();
        series->data->data = new REAL4[series->data->length];        
        for(int i = 0; i < series->data->length; i++)
            series->data->data[i] = y[i];      

        return series;
    }

    inline REAL8FrequencySeries* __REAL8FrequencySeries(TString name, std::vector<double> &y, double dF, double GPS = 0) 
    {
        REAL8FrequencySeries *series = new REAL8FrequencySeries;
        for(int i = 0, N = LALNameLength; i < TMath::Min(TString(name).Length(), N); i++)
            series->name[i] = name[i];

        unsigned int GTimeS = (unsigned int) GPS;
        unsigned int GTimeN = 1e9 * (GPS - GTimeS);
        series->epoch = {static_cast<INT4>(GTimeS), static_cast<INT4>(GTimeN)};
        
        series->f0 = 0;
        series->deltaF = dF;
        series->sampleUnits = lalSecondUnit;

        series->data = new REAL8Sequence;       
        series->data->length = y.size() % 2 == 0 ? y.size()/2 + 1 : y.size();
        series->data->data = new REAL8[series->data->length];        
        for(int i = 0; i < series->data->length; i++)
            series->data->data[i]   = y[i];

        series->data->data[0] = 0; // @TODO: DC component? Set to zero due to an error message (maybe a mistake somewhere?)
        return series;
    }

    inline REAL4FrequencySeries* __REAL4FrequencySeries(TString name, std::vector<float> &y, double dF, double GPS = 0) 
    {
        REAL4FrequencySeries *series = new REAL4FrequencySeries;

        for(int i = 0, N = LALNameLength; i < TMath::Min(TString(name).Length(), N); i++)
            series->name[i] = name[i];

        unsigned int GTimeS = (unsigned int) GPS;
        unsigned int GTimeN = 1e9 * (GPS - GTimeS);
        series->epoch = {static_cast<INT4>(GTimeS), static_cast<INT4>(GTimeN)};
        
        series->f0 = 0;
        series->deltaF = dF;
        series->sampleUnits = lalSecondUnit;

        series->data = new REAL4Sequence;
        series->data->length = y.size() % 2 == 0 ? y.size()/2 + 1 : y.size();
        series->data->data = new REAL4[series->data->length];        
        for(int i = 0; i < series->data->length; i++)
            series->data->data[i]   = y[i];

        series->data->data[0] = 0; // @TODO: DC component? Set to zero due to an error message (maybe a mistake somewhere?)
        return series;
    }

    //
    // LAL struct to ROOT histogram methods
    template<typename T>
    inline static TH1* __TH1(TString name, TString title, std::vector<double> x, std::vector<T> content, std::vector<T> contentErr = std::vector<T>())
    {
            if(x.size() == 0) return NULL;
            if(x.size()-1 != content.size()) {
                    
                    TString hintStr = "[#x=`"+TString::Itoa(x.size(),10)+"`,#content=`"+TString::Itoa(content.size(),10)+"`] ";
                    if(x.size() == content.size()) hintStr += "(did you added x range upper bound?)";

                    gPrint->Error(__METHOD_NAME__, (TString) "Wrong vector axis size.. cannot compute histogram `"+name+"` %s", hintStr.Data());
                    return NULL;
            }

            if(contentErr.size() > 0 && content.size() != contentErr.size()) {
                    
                    TString hintStr = "[#x=`"+TString::Itoa(x.size(),10)+"`,#content=`"+TString::Itoa(content.size(),10)+"`,#sigma`"+TString::Itoa(contentErr.size(),10)+"`]";
                    gPrint->Error(__METHOD_NAME__, (TString) "Wrong uncertainty vector size.. cannot compute histogram `"+name+"` %s", hintStr.Data());
                    return NULL;
            }
            
            TH1* h = NULL;
            if(std::is_same<T,char>::value)
                    h = (TH1*) new TH1C(name, title, x.size()-1, &x[0]);
            if(std::is_same<T,short>::value)
                    h = (TH1*) new TH1S(name, title, x.size()-1, &x[0]);
            if(std::is_same<T,int>::value)
                    h = (TH1*) new TH1I(name, title, x.size()-1, &x[0]);
            if(std::is_same<T,float>::value)
                    h = (TH1*) new TH1F(name, title, x.size()-1, &x[0]);
            if(std::is_same<T,double>::value)
                    h = (TH1*) new TH1D(name, title, x.size()-1, &x[0]);
            
            if(h == NULL) return NULL;

            h->Sumw2(false);

            for(int i = 1, N = h->GetNbinsX()+1, contentErrN = contentErr.size(); i < N; i++) 
            {
                    int idx = i-1;
                    h->SetBinContent(i, content[idx]);
                    if(contentErrN > 0) h->SetBinError(i, contentErr[idx]);
            }

            return h;
    }

    template <typename T>
    inline std::vector<T> __axis(unsigned int N, T dx)
    {
            std::vector<T> v;
            for(unsigned int i = 0; i < N; i++)
                    v.push_back(i*dx);

            return v;
    }

    template <typename T>
    inline std::vector<T> __axis(unsigned int N, T min, T max)
    {
            if(N == 0) return {};
            if(N == 1) return {min, max};

            std::vector<T> v;
            T dx = (max-min)/(N-1);
            for(unsigned int i = 0; i < N; i++)
                    v.push_back(min + i*dx);

            return v;
    }
    
    inline TH1D* __TH1(REAL8TimeSeries *series) 
    {
        double GPS = gClockGPS->Timestamp(series->epoch.gpsSeconds, series->epoch.gpsNanoSeconds);
        double length = series->data->length*series->deltaT;

        std::vector<double> axis =__axis(series->data->length+1, GPS, GPS + length);
        return (TH1D*) __TH1(series->name, Form("REAL8TimeSeries %f", GPS), axis, __vector(series));
    }

    inline TH1F* __TH1(REAL4TimeSeries *series) 
    {
        double GPS = gClockGPS->Timestamp(series->epoch.gpsSeconds, series->epoch.gpsNanoSeconds);
        double length = series->data->length*series->deltaT;

        std::vector<double> axis =__axis(series->data->length+1, GPS, GPS + length);
        return (TH1F*) __TH1(series->name, Form("REAL4TimeSeries %f", GPS), axis, __vector(series));
    }

    inline TH1D* __TH1(REAL8FrequencySeries *series) 
    {
        double GPS = gClockGPS->Timestamp(series->epoch.gpsSeconds, series->epoch.gpsNanoSeconds);
        double length = series->data->length*series->deltaF;

        std::vector<double> axis =__axis(series->data->length+1, 0.0, length);
        return (TH1D*) __TH1(series->name, Form("REAL8FrequencySeries %f", GPS), axis, __vector(series));
    }

    inline TH1F* __TH1(REAL4FrequencySeries *series) 
    {
        double GPS = gClockGPS->Timestamp(series->epoch.gpsSeconds, series->epoch.gpsNanoSeconds);
        double length = series->data->length*series->deltaF;

        std::vector<double> axis =__axis(series->data->length+1, 0.0, length);
        return (TH1F*) __TH1(series->name, Form("REAL4FrequencySeries %f", GPS), axis, __vector(series));
    }

    inline std::pair<TH1D*,TH1D*> __TH1(COMPLEX16TimeSeries *series) 
    {
        double GPS = gClockGPS->Timestamp(series->epoch.gpsSeconds, series->epoch.gpsNanoSeconds);
        double length = series->data->length*series->deltaT;

        std::vector<double> axis =__axis(series->data->length+1, GPS, GPS+length);

        std::vector<std::complex<double>> v = __vector(series);
        std::vector<double> re(v.size()), im(v.size());
        for(int i = 0, N = v.size(); i < N; i++) {

            re[i] = v[i].real();
            im[i] = v[i].imag();
        }

        return std::make_pair(
            (TH1D*) __TH1(TString(series->name), Form("COMPLEX16TimeSeries %f", GPS), axis, re),
            (TH1D*) __TH1(TString(series->name), Form("COMPLEX16TimeSeries %f", GPS), axis, im)
        );
    }

    inline std::pair<TH1F*,TH1F*> __TH1(COMPLEX8TimeSeries *series) 
    {
        double GPS = gClockGPS->Timestamp(series->epoch.gpsSeconds, series->epoch.gpsNanoSeconds);
        double length = series->data->length*series->deltaT;

        std::vector<double> axis =__axis(series->data->length+1, GPS, GPS+length);

        std::vector<std::complex<float>> v = __vector(series);
        std::vector<double> re(v.size()), im(v.size());
        for(int i = 0, N = v.size(); i < N; i++) {

            re[i] = v[i].real();
            im[i] = v[i].imag();
        }

        return std::make_pair(
            (TH1F*) __TH1(TString(series->name), Form("COMPLEX8TimeSeries %f", GPS), axis, re),
            (TH1F*) __TH1(TString(series->name), Form("COMPLEX8TimeSeries %f", GPS), axis, im)
        );
    }

    inline std::pair<TH1D*,TH1D*> __TH1(COMPLEX16FrequencySeries *series) 
    {
        double GPS = gClockGPS->Timestamp(series->epoch.gpsSeconds, series->epoch.gpsNanoSeconds);
        double length = series->data->length*series->deltaF;

        std::vector<double> axis =__axis(series->data->length+1, 0.0, length);

        std::vector<std::complex<double>> v = __vector(series);
        std::vector<double> re(v.size()), im(v.size());
        for(int i = 0, N = v.size(); i < N; i++) {

            re[i] = v[i].real();
            im[i] = v[i].imag();
        }

        return std::make_pair(
            (TH1D*) __TH1(TString(series->name), Form("COMPLEX16FrequencySeries %f", GPS), axis, re),
            (TH1D*) __TH1(TString(series->name), Form("COMPLEX16FrequencySeries %f", GPS), axis, im)
        );
    }

    inline std::pair<TH1F*,TH1F*> __TH1(COMPLEX8FrequencySeries *series) 
    {
        double GPS = gClockGPS->Timestamp(series->epoch.gpsSeconds, series->epoch.gpsNanoSeconds);
        double length = series->data->length*series->deltaF;

        std::vector<double> axis =__axis(series->data->length+1, 0.0, length);

        std::vector<std::complex<float>> v = __vector(series);
        std::vector<double> re(v.size()), im(v.size());
        for(int i = 0, N = v.size(); i < N; i++) {

            re[i] = v[i].real();
            im[i] = v[i].imag();
        }

        return std::make_pair(
            (TH1F*) __TH1(TString(series->name), Form("COMPLEX8FrequencySeries %f", GPS), axis, re),
            (TH1F*) __TH1(TString(series->name), Form("COMPLEX8FrequencySeries %f", GPS), axis, im)
        );
    }
}
