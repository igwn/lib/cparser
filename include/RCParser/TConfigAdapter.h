#pragma once

#include "RCParser/TConfigNode.h"
#include "libRCParser.Config.h"

#include <TString.h>

class TConfigAdapter {

    public:

        virtual ~TConfigAdapter() {}

        virtual const char *GetName() = 0;

        // Load configuration from file and return as TConfigNode
        virtual TConfigNode *Load(const char *fName);

        // Parse content into a TConfigNode
        virtual TConfigNode *Parse(const char *contents, int size) = 0;

        // Save TConfigNode to file
        virtual void Save(const char *filename, const TConfigNode& root) = 0;

        // Check if the adapter supports the given format based on file content
        virtual bool Supports(const char *filename) = 0;
};