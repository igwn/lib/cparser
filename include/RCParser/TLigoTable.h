#pragma once


#include <Rioplus.h>
#include <TPrint.h>
#include <TFileReader.h>


#include <TObject.h>
#include <TMap.h>

#include <math.h>
#include <stdlib.h>
#include <tuple>
#include <fstream>

class TLigoTable: public TNamed
{
    public:

        enum class Type { undefined, int_8u, int_4u, int_8s, int_4s, real_8, real_4, lstring };
        static void enum2str() { std::invalid_argument("Invalid argument converting enum to string"); }
        static TString enum2str(Type ptr)
        {
            switch(ptr)
            {
                case Type::undefined: return "undefined";
                case Type::int_8u   : return "int_8u";
                case Type::int_4u   : return "int_4u";
                case Type::int_8s   : return "int_8s";
                case Type::int_4s   : return "int_4s";
                case Type::real_8   : return "real_8";
                case Type::real_4   : return "real_4";
                case Type::lstring  : return "lstring";
            }

            enum2str();
        }

        static Type str2type(TString str)
        {
            if(str == "undefined") return Type::undefined;
            if(str == "int_8u"   ) return Type::int_8u;
            if(str == "int_4u"   ) return Type::int_4u;
            if(str == "int_8s"   ) return Type::int_8s;
            if(str == "int_4s"   ) return Type::int_4s;
            if(str == "real_8"   ) return Type::real_8;
            if(str == "real_4"   ) return Type::real_4;
            if(str == "lstring"  ) return Type::lstring;

            throw std::invalid_argument("Invalid data class name provided ("+str+").");
            return Type::undefined;
        }

        using TableDict     = std::map<TString, Type>;
        using Table         = std::map<TString, TString>;

        using TableDictIter = TableDict::iterator;
        using TableIter     = Table::iterator;

    protected:

        int entries;
        TableDict dict;
        Table map;

    public:

        TLigoTable(const char *name, const char *title = ""): TNamed(name, title) { };
        TLigoTable(const TString &name, const TString &title = ""): TNamed(name, title) { };
        ~TLigoTable() 
        {
            map.clear();
            dict.clear();
        }

        using TNamed::Print;
        void Print(Option_t * opt = "");

        bool HasColumn(TString key);
        void AddColumn(TString key, Type type);
        void RemoveColumn(TString key);

        Type GetType(TString key);
        void SetType(TString key, Type type);

        std::vector<TString> GetColumns();
        int GetEntries(TString key);
        int GetEntries();

        TString Get(TString key, int index);
        bool Has(TString key, int index);
        void Add(TString key, TString value);

    ClassDef(TLigoTable, 1);
};