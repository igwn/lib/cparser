#pragma once 

#include <Riostream.h>
#include <TObject.h>

#include <TConfig.h>
#include <TFileReader.h>
#include <TCli.h>

#include "RCParser/TLigoTable.h"

#ifndef _FILEIO_H
    struct tagLALFILE {
    int compression;
    void *fp;
    }; // struct is defined into ./lal/lib/support/FileIO.c.. common..
    #include <lal/FileIO.h>
#endif

#include <math.h>
#include <stdlib.h>
#include <lal/LIGOLwXML.h>
#include <lal/LIGOLwXMLArray.h>
#include <lal/LALStdlib.h>
#include <lal/FrequencySeries.h>
#include <lal/TimeSeries.h>
#include <lal/Units.h>
#include <lal/LALSimNoise.h>
#include <lal/Date.h>

#include <TXMLDocument.h>
#include <TXMLNode.h>
#include <TXMLAttr.h>

#include <tuple>
#include <fstream>

class TLigoFile: public TFile
{
    private:

        enum class PTR_NODE { Empty, LIGO_LW, PCDATA, CDATA, Comment, Column, Dim, Param, Table, Array, Stream, IGWDFrame, AdcData, AdcInterval, Time, Detector };
        using TLigoNode       = std::pair<TLigoFile::PTR_NODE, void*>;
        using TLigoAttributes = std::map<TString, TString>;
        using TLigoElements   = std::vector<TLigoNode>;

        static void enum2str() { std::invalid_argument("Invalid argument converting enum to string"); }
        static TString enum2str(PTR_NODE ptr)
        {
            switch(ptr)
            {
                case PTR_NODE::Empty      : return "Empty";
                case PTR_NODE::LIGO_LW    : return "LIGO_LW";
                case PTR_NODE::PCDATA     : return "PCDATA";
                case PTR_NODE::CDATA      : return "CDATA";
                case PTR_NODE::Comment    : return "Comment";
                case PTR_NODE::Column     : return "Column";
                case PTR_NODE::Param      : return "Param";
                case PTR_NODE::Table      : return "Table";
                case PTR_NODE::Array      : return "Array";
                case PTR_NODE::Dim        : return "Dim";
                case PTR_NODE::Stream     : return "Stream";
                case PTR_NODE::IGWDFrame  : return "IGWDFrame";
                case PTR_NODE::AdcData    : return "AdcData";
                case PTR_NODE::AdcInterval: return "AdcInterval";
                case PTR_NODE::Time       : return "Time";
                case PTR_NODE::Detector   : return "Detector";
            }

            enum2str();
        }

        static PTR_NODE str2node(TString str)
        {
            if(str == "Empty"      ) return PTR_NODE::Empty      ;
            if(str == "LIGO_LW"    ) return PTR_NODE::LIGO_LW    ;
            if(str == "PCDATA"     ) return PTR_NODE::PCDATA     ;
            if(str == "CDATA"      ) return PTR_NODE::CDATA      ;
            if(str == "Comment"    ) return PTR_NODE::Comment    ;
            if(str == "Param"      ) return PTR_NODE::Param      ;
            if(str == "Column"     ) return PTR_NODE::Column     ; 
            if(str == "Table"      ) return PTR_NODE::Table      ; 
            if(str == "Dim"        ) return PTR_NODE::Dim        ; 
            if(str == "Array"      ) return PTR_NODE::Array      ;
            if(str == "Stream"     ) return PTR_NODE::Stream     ; 
            if(str == "IGWDFrame"  ) return PTR_NODE::IGWDFrame  ;
            if(str == "AdcData"    ) return PTR_NODE::AdcData    ; 
            if(str == "AdcInterval") return PTR_NODE::AdcInterval;
            if(str == "Time"       ) return PTR_NODE::Time       ;
            if(str == "Detector"   ) return PTR_NODE::Detector   ;

            throw std::invalid_argument("Invalid pointer class name provided ("+str+").");
            return PTR_NODE::LIGO_LW;
        }
        
        enum class Series {
                COMPLEX16TimeSeries, COMPLEX16FrequencySeries,
                REAL8TimeSeries, REAL8FrequencySeries,
                COMPLEX8TimeSeries, COMPLEX8FrequencySeries,
                REAL4TimeSeries, REAL4FrequencySeries
        };

        static TString enum2str(Series series)
        {
            switch(series)
            {
                case Series::COMPLEX16TimeSeries: return "COMPLEX16TimeSeries";
                case Series::COMPLEX16FrequencySeries: return "COMPLEX16FrequencySeries";
                
                case Series::REAL8TimeSeries: return "REAL8TimeSeries";
                case Series::REAL8FrequencySeries: return "REAL8FrequencySeries";
                
                case Series::COMPLEX8TimeSeries: return "COMPLEX8TimeSeries";
                case Series::COMPLEX8FrequencySeries: return "COMPLEX8FrequencySeries";
                
                case Series::REAL4TimeSeries: return "REAL4TimeSeries";
                case Series::REAL4FrequencySeries: return "REAL4FrequencySeries";
            }

            enum2str();
        }

        static Series str2series(TString str)
        {
            if(str == "COMPLEX16TimeSeries") return Series::COMPLEX16TimeSeries;
            if(str == "COMPLEX16FrequencySeries") return Series::COMPLEX16FrequencySeries;
            if(str == "REAL8TimeSeries") return Series::REAL8TimeSeries;
            if(str == "REAL8FrequencySeries") return Series::REAL8FrequencySeries;
            if(str == "COMPLEX8TimeSeries") return Series::COMPLEX8TimeSeries;
            if(str == "COMPLEX8FrequencySeries") return Series::COMPLEX8FrequencySeries;
            if(str == "REAL4TimeSeries") return Series::REAL4TimeSeries;
            if(str == "REAL4FrequencySeries") return Series::REAL4FrequencySeries;

            throw std::invalid_argument("Invalid pointer class name provided ("+str+").");
            return Series::REAL8TimeSeries;
        }

        enum class StreamType { Remote,Local };
        static TString enum2str(StreamType ptr)
        {
            switch(ptr)
            {
                case StreamType::Remote    : return "Remote";
                case StreamType::Local     : return "Local";
            }

            enum2str();
        }

        static StreamType str2stream(TString str)
        {
            if(str == "Remote"    ) return StreamType::Remote   ;
            if(str == "Local"     ) return StreamType::Local;

            throw std::invalid_argument("Invalid pointer class name provided ("+str+").");
            return StreamType::Local;
        }

        enum TimeType {GPS, Unix, ISO8601};
        static TString enum2str(TimeType ptr)
        {
            switch(ptr)
            {
                case TimeType::GPS    : return "GPS";
                case TimeType::Unix     : return "Unix";
                case TimeType::ISO8601      : return "ISO-8601";
            }

            enum2str();
        }

        static TimeType str2time(TString str)
        {
            if(str == "GPS"    ) return TimeType::GPS    ;
            if(str == "Unix"     ) return TimeType::Unix     ;
            if(str == "ISO-8601"      ) return TimeType::ISO8601      ;

            std::invalid_argument("Invalid pointer class name provided ("+str+").");
            return TimeType::GPS;
        }

        struct CDATA  { TString Data; };
        struct PCDATA { std::vector<TString> Data; };

        struct LIGO_LW
        {
            // <!ELEMENT LIGO_LW ((LIGO_LW|Comment|Param|Table|Array|Stream|IGWDFrame|AdcData|AdcInterval|Time|Detector)*)>
            TLigoElements _self;

            // <!ATTLIST LIGO_LW
            //           Name CDATA #IMPLIED
            //           Type CDATA #IMPLIED>
            CDATA Name;
            CDATA Type;
        };

        struct Comment
        {
            // <!ELEMENT Comment (#PCDATA)>
            PCDATA _self;
        };

        struct Param
        {
            // <!ELEMENT Param (#PCDATA|Comment)*>
            PCDATA _self;

            // <!ATTLIST Param 
            //           Name CDATA #IMPLIED
            //           Type CDATA #IMPLIED
            //           Start CDATA #IMPLIED
            //           Scale CDATA #IMPLIED
            //           Unit CDATA #IMPLIED
            //           DataUnit CDATA #IMPLIED>
            CDATA Name;
            CDATA Type;
            CDATA Start;
            CDATA Scale;
            CDATA Unit;
            CDATA DataUnit;
        };

        struct Table
        {
            // <!ELEMENT Table (Comment?,Column*,Stream?)>
            TLigoElements _self;

            // <!ATTLIST Table 
            //           Name CDATA #IMPLIED
            //           Type CDATA #IMPLIED>
            CDATA Name;
            CDATA Type;
        };

        struct Column
        {
            // <!ELEMENT Column Empty>
            // <!ATTLIST Column
            //           Name CDATA #IMPLIED
            //           Type CDATA #IMPLIED
            //           Unit CDATA #IMPLIED>
            CDATA Name;
            CDATA Type;
            CDATA Unit;
        };

        struct Array
        {
            // <!ELEMENT Array (Dim*,Stream?)>
            TLigoElements _self;

            // <!ATTLIST Array 
            //           Name CDATA #IMPLIED
            //           Type CDATA #IMPLIED
            //           Unit CDATA #IMPLIED>
            CDATA Name;
            CDATA Type;
            CDATA Unit;
        };

        struct Dim
        {
            // <!ELEMENT Dim (#PCDATA)>
            PCDATA _self;

            // <!ATTLIST Dim 
            //           Name  CDATA #IMPLIED
            //           Unit CDATA #IMPLIED
            //           Start CDATA #IMPLIED
            //           Scale CDATA #IMPLIED>
            CDATA Name;
            CDATA Unit;
            CDATA Start;
            CDATA Scale;
        };

        struct Stream
        {
            // <!ELEMENT Stream (#PCDATA)>
            PCDATA _self;

            // <!ATTLIST Stream 
            //           Name      CDATA #IMPLIED
            //           Type      (Remote|Local) "Local"
            //           Delimiter CDATA ","
            //           Encoding  CDATA #IMPLIED
            //           Content   CDATA #IMPLIED>
            CDATA Name;
            StreamType Type;

            CDATA Delimiter;
            CDATA Encoding;
            CDATA Content;
        };

        struct IGWDFrame
        {
            // <!ELEMENT IGWDFrame ((Comment|Param|Time|Detector|AdcData|LIGO_LW|Stream?|Array|IGWDFrame)*)>
            TLigoElements _self;

            // <!ATTLIST IGWDFrame 
            //           Name CDATA #IMPLIED>
            CDATA Name;
        };

        struct Detector
        {
            // <!ELEMENT Detector ((Comment|Param|LIGO_LW)*)>
            TLigoElements _self;

            // <!ATTLIST Detector 
            //           Name CDATA #IMPLIED>
            CDATA Name;
        };

        struct AdcData
        {
            // <!ELEMENT AdcData ((AdcData|Comment|Param|Time|LIGO_LW|Array)*)>
            TLigoElements _self;

            // <!ATTLIST AdcData 
            //           Name CDATA #IMPLIED>
            CDATA Name;
        };
        
        struct AdcInterval
        {
            // <!ELEMENT AdcInterval ((AdcData|Comment|Time)*)>
            TLigoElements _self;

            // <!ATTLIST AdcInterval 
            //           Name CDATA #IMPLIED
            //           StartTime CDATA #IMPLIED
            //           DeltaT CDATA #IMPLIED>
            CDATA Name;
            CDATA StartTime;
            CDATA DeltaT;
        };

        struct Time
        {
            // <!ELEMENT Time (#PCDATA)>
            PCDATA _self;

            // <!ATTLIST Time 
            //           Name CDATA #IMPLIED
            //           Type (GPS|Unix|ISO-8601) "ISO-8601">
            CDATA Name;
            TimeType Type;
        };
    
        protected:

            TLigoNode       ParseXMLNode      (TXMLNode *node);
            TLigoElements   ParseXMLElements  (TXMLNode *node);
            TLigoAttributes ParseXMLAttributes(TXMLNode *node);

            void ParseXMLNode      (TXMLNode *node, TLigoNode &ptr);
            void ParseXMLElements  (TXMLNode *node, TLigoNode &ptr);
            void ParseXMLAttributes(TXMLNode *node, TLigoNode &ptr);
            
            //NB: Complex series are not implemented yet
            template<typename T, typename std::enable_if<std::is_same<T, REAL8FrequencySeries>::value || std::is_same<T, REAL4FrequencySeries>::value || std::is_same<T, REAL8TimeSeries>::value || std::is_same<T, REAL4TimeSeries>::value, int>::type = 0>
            T* Cast(TLigoNode &node);
            template<typename T, typename std::enable_if<std::is_same<T, REAL8FrequencySeries>::value || std::is_same<T, REAL4FrequencySeries>::value || std::is_same<T, REAL8TimeSeries>::value || std::is_same<T, REAL4TimeSeries>::value, int>::type = 0>
            T* Cast(LIGO_LW *ligoLw);
            
            template<typename T, typename std::enable_if<std::is_same<T, REAL8FrequencySeries>::value, int>::type = 0>
            T* Cast(Array *array);
            template<typename T, typename std::enable_if<std::is_same<T, REAL4FrequencySeries>::value, int>::type = 0>
            T* Cast(Array *array);
            template<typename T, typename std::enable_if<std::is_same<T, REAL8TimeSeries>::value, int>::type = 0>
            T* Cast(Array *array);
            template<typename T, typename std::enable_if<std::is_same<T, REAL4TimeSeries>::value, int>::type = 0>
            T* Cast(Array *array);

            std::map<TString, TString> GetParameters(LIGO_LW *ligoLw);
            TDirectory *GetRelativeIndexDirectory(LIGO_LW *ligoLw, std::vector<TString> indexers = {}, TDirectory *relativeDir = NULL);

            std::vector<Dim *> Cast(Array *array);

            TLigoTable *Cast(Table *table);
            
            template<typename T>
            std::vector<std::vector<T>> Cast(Stream *stream);
            template<typename T>
            std::vector<std::vector<T>> Axis(Stream *stream);

            TLigoElements FindChildByNode   (TLigoNode &node, PTR_NODE validDataClass);
            TLigoElements FindChildByNode   (void *ptr, PTR_NODE dataClass, PTR_NODE validDataClass);
            TLigoNode     FindOneChildByNode(TLigoNode &node, PTR_NODE validDataClass);
            TLigoNode     FindOneChildByNode(void *ptr, PTR_NODE dataClass, PTR_NODE validDataClass);

            int WriteXML(const char * xmlFile, LIGOLwXMLStream *xml, TDirectory *directory);
            int WriteXML(const char * xmlFile, LIGOLwXMLStream *xml, TH1  *histogram);
            int WriteXML(const char * xmlFile, LIGOLwXMLStream *xml, TLigoTable *map);

            TString DTD;

        public:

            using TFile::TFile;
            using TFile::Open;
            using TFile::Print;

            bool Parse(const char *fname, TString title = "", TString pattern = "", std::vector<TString> indexers = {});
            bool ParseXML(const char *xmlFile, TString title = "", TString pattern = "", std::vector<TString> indexers = {});

            int Write(const char *name=nullptr, int opt=0, int bufsiz=0);
            int WriteXML(const char *xmlFile);

            void SetDTD(TString DTD) { this->DTD = DTD; }
            TString GetDTD() { return this->DTD; }

        ClassDef(TLigoFile, 1);
};