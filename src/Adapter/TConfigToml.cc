#include "RCParser/Adapter/TConfigToml.h"

#ifdef TOML_FOUND
#include <toml++/toml.hpp>
#include <sstream>
#include <string.h>

TConfigNode *TConfigToml::Parse(const char *contents, int size) {
    
    UNUSED(size);
    
    toml::table tbl;
    if (contents != NULL && contents[0] == '\0') tbl = toml::table();
    else {

        std::stringstream ss{ std::string{ contents } };
        
        try { tbl = toml::parse(ss); }
        catch (...)
        {
            std::cerr << "Failed to parse: `" << gPrint->MakeItShorter(contents, 25) << "`"; 
            tbl = toml::table();
        }
    }

    std::function<TConfigNode*(const char *, const toml::node&)> _callback = [&_callback](const char *tag, const toml::node& tbl) mutable {

        TConfigNode *node = new TConfigNode(tag);
                     node->SetIndexer(TConfigNode::Indexer::Dictionary);

        if (tbl.is_table()) {

            for (const auto& [key, value] : *tbl.as_table()) {

                TConfigNode *child = _callback(key.data(), value);
                node->Attach(*child);
            }

        } else if (tbl.is_array()) {

            int i = 0;
            for (const auto& item : *tbl.as_array()) {

                TConfigNode *child = _callback(TString::Itoa(i++,10), item);
                node->Attach(*child);
            }

            node->SetIndexer(TConfigNode::Indexer::Array);

        } else {

            // Simple type conversion
            if (tbl.is_string()) node->SetContent(tbl.value_or(std::string("")));
            else if (tbl.is_integer()) node->SetContent(static_cast<int>(tbl.value_or(0)));
            else if (tbl.is_floating_point()) node->SetContent(tbl.value_or(0.0));
            else if (tbl.is_boolean()) node->SetContent(tbl.value_or(false));
            // Extend to other types as needed
        }

        return node;
    };

    return _callback("root", tbl);
}

void TConfigToml::Save(const char *filename, const TConfigNode& root) {
    
    std::ofstream file(filename);
    if (!file) {
        std::cerr << "Unable to open " << filename << " for writing.\n";
        return;
    }

    // Lambda function for recursive serialization
    std::function<void(const TConfigNode&, toml::table&)> _callback_writer = [&](const TConfigNode& node, toml::table& parentTable) {

        for (const auto& childNode : node.GetElements()) {

            if (childNode->GetType() == TConfigNode::Type::Empty) {

                if (childNode->IsArray()) {

                    toml::array arr; // Assuming this has been filled somewhere
                    // Assume all elements in the array are of the same type for simplicity
                    for (const auto& arrayItem : childNode->GetElements()) {

                        TConfigNode::Value content = arrayItem->GetContent();
                        if (auto str = std::get_if<TString>(&content)) {
                            arr.push_back(str->Data());
                        } else if (auto i = std::get_if<int>(&content)) {
                            arr.push_back(*i);
                        } else if (auto f = std::get_if<float>(&content)) {
                            arr.push_back(*f);
                        } else if (auto d = std::get_if<double>(&content)) {
                            arr.push_back(*d);
                        }
                    }
                    parentTable.insert_or_assign(childNode->GetTag().Data(), arr);

                } else if (childNode->IsDictionary()) {
                
                    toml::table tbl;
                    _callback_writer(*childNode, tbl); // Recursive call to handle nested structure
                    parentTable.insert_or_assign(childNode->GetTag().Data(), tbl);
                }

            } else {

                TConfigNode::Value content = childNode->GetContent();
                if (auto str = std::get_if<TString>(&content)) {
                    parentTable.insert_or_assign(childNode->GetTag().Data(), str->Data());
                } else if (auto i = std::get_if<int>(&content)) {
                    parentTable.insert_or_assign(childNode->GetTag().Data(), *i);
                } else if (auto f = std::get_if<float>(&content)) {
                    parentTable.insert_or_assign(childNode->GetTag().Data(), *f);
                } else if (auto d = std::get_if<double>(&content)) {
                    parentTable.insert_or_assign(childNode->GetTag().Data(), *d);
                } else if (auto b = std::get_if<bool>(&content)) {
                    parentTable.insert_or_assign(childNode->GetTag().Data(), *b);
                }
            }
        }
    };

    toml::table rootTable;
    _callback_writer(root, rootTable);
    file << rootTable;

}

bool TConfigToml::Supports(const char *filename) {
    return TString(filename).EndsWith(".toml");
}

#endif