#include "RCParser/Adapter/TConfigXml.h"

#ifdef XML_FOUND

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

TConfigNode *TConfigXml::Parse(const char *contents, int size) {
LIBXML_TEST_VERSION

    xmlDocPtr doc = xmlReadMemory(contents, size, "noname.xml", nullptr, 0);
    if (doc == nullptr) {
        std::cerr << "Failed to parse XML\n";
        return nullptr;
    }

    xmlNode* rootNode = xmlDocGetRootElement(doc);
    if (rootNode == nullptr) {
        std::cerr << "Empty XML document\n";
        xmlFreeDoc(doc);
        return nullptr;
    }

    // Define the recursive lambda function for node parsing
    std::function<TConfigNode*(xmlNode*)> parseNode = [&](xmlNode* node) -> TConfigNode* {
        if (node == nullptr) return nullptr;

        TConfigNode* newNode = new TConfigNode(reinterpret_cast<const char*>(node->name));

        // Parse attributes
        for (xmlAttr* attr = node->properties; attr != nullptr; attr = attr->next) {
            xmlChar* value = xmlGetProp(node, attr->name);
            if (value) {
                newNode->AddAttribute(reinterpret_cast<const char*>(attr->name), reinterpret_cast<const char*>(value));
                xmlFree(value);
            }
        }

        // Handle child nodes
        for (xmlNode* child = node->children; child; child = child->next) {
            if (child->type == XML_ELEMENT_NODE) {

                TConfigNode* childNode = parseNode(child);
                if (childNode) {

                    if (childNode->GetTag().EqualTo("item") and newNode->IsArray()) {
                        childNode->SetTag(TString::Itoa(newNode->GetN(),10));
                    }

                    newNode->Attach(*childNode);
                }

            } else if (child->type == XML_TEXT_NODE && xmlNodeGetContent(child) && newNode->GetN() < 1) {
                newNode->SetContent(reinterpret_cast<const char*>(xmlNodeGetContent(child)));
            }
        }

        return newNode;
    };

    TConfigNode* parsedRoot = parseNode(rootNode);

    xmlFreeDoc(doc);
    xmlCleanupParser();

    return parsedRoot;
}

void TConfigXml::Save(const char *filename, const TConfigNode& root) {

 // Initialize a new XmlWriter for memory, with no compression.
    xmlBufferPtr buf = xmlBufferCreate();
    if (buf == nullptr) {
        std::cerr << "Failed to create XML buffer\n";
        return;
    }

    xmlTextWriterPtr writer = xmlNewTextWriterMemory(buf, 0);
    if (writer == nullptr) {
        std::cerr << "Failed to create XML writer\n";
        xmlBufferFree(buf); // Clean up buffer on failure
        return;
    }

    xmlTextWriterStartDocument(writer, nullptr, "UTF-8", nullptr);
    xmlTextWriterSetIndent(writer, 1); // Enable indentation
    xmlTextWriterSetIndentString(writer, BAD_CAST "    "); // Use tab for indentation

    // Recursive function to write TConfigNode and its children
    std::function<void(xmlTextWriterPtr, const TConfigNode&)> writeNode;
    writeNode = [&](xmlTextWriterPtr writer, const TConfigNode& node) {

        // Start the element
        if(node.GetParent() && node.GetParent()->IsArray()) xmlTextWriterStartElement(writer, BAD_CAST "item");
        else xmlTextWriterStartElement(writer, BAD_CAST node.GetTag().Data());

        // Write attributes
        auto attributes = node.GetAttributes();
        for (const auto& attr : attributes) {
            xmlTextWriterWriteAttribute(writer, BAD_CAST attr.first.Data(), BAD_CAST attr.second.Data());
        }

        // Determine content or child nodes
        if (!node.GetElements().empty()) {

            // Write child nodes
            for (const auto& child : node.GetElements()) {
                writeNode(writer, *child);
            }

        } else {

            TConfigNode::Value content = node.GetContent();
            if (auto str = std::get_if<TString>(&content)) {
                xmlTextWriterWriteString(writer, BAD_CAST str->Data());
            } else if (auto i = std::get_if<int>(&content)) {
                xmlTextWriterWriteString(writer, BAD_CAST Form("%d", *i));
            } else if (auto f = std::get_if<float>(&content)) {
                xmlTextWriterWriteString(writer, BAD_CAST Form("%g", *f));
            } else if (auto d = std::get_if<double>(&content)) {
                xmlTextWriterWriteString(writer, BAD_CAST Form("%f", *d));
            } else if (auto b = std::get_if<bool>(&content)) {
                xmlTextWriterWriteString(writer, BAD_CAST (*b ? "true" : "false"));
            }
        }

        // Close the element
        xmlTextWriterEndElement(writer);
    };

    // Start writing from the root node
    writeNode(writer, root);

    xmlTextWriterEndDocument(writer);

    // Save XML content to the file
    std::ofstream outFile(filename, std::ios::out | std::ios::binary);
    if (outFile.is_open()) {
        outFile.write((const char*)buf->content, buf->use);
        outFile.close();
    } else {
        std::cerr << "Failed to open file for writing: " << filename << "\n";
    }

    // Cleanup
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
}

bool TConfigXml::Supports(const char *filename) {
    return TString(filename).EndsWith(".xml");
}

#endif