#include "RCParser/TConfigNode.h"
#include "Rioplus.h"
#include <algorithm>

TConfigNode::TConfigNode() : tag("root"), parent(nullptr) {}
TConfigNode::~TConfigNode()
{     
    for(auto *node : this->elements) {
        node->parent = nullptr;
        delete node;
    }

    elements.clear();
}

TConfigNode::TConfigNode(const TString& tag) : tag(tag), parent(nullptr) {}

TConfigNode::TConfigNode(const TConfigNode &node)
{
    this->SetTag(node.GetTag());

    this->SetContent(node.GetContent());
    this->SetIndexer(node.GetIndexer());

    for(auto element : node.GetElements()) {
        this->Attach(*(new TConfigNode(*element)));
    }

    for(auto attribute : node.GetAttributes()) {
        this->AddAttribute(attribute.first, attribute.second);
    }
}

void TConfigNode::Attach(TConfigNode& element) {

    if (&element == this || IsAncestorOf(element)) {
        std::cerr << "Warning: cyclic loop `" << element.GetTag() << "` detected in the tree structure. skip." << std::endl;
        return;
    }

    if (element.parent) {
        element.parent->Detach(element);
    }

    element.parent = this;
    elements.push_back(&element);
}

TConfigNode& TConfigNode::operator=(const Value& content) {

    this->SetContent(content);
    return *this;
}

TConfigNode::Indexer TConfigNode::GetIndexer() const
{
    return indexer;
}

TConfigNode *TConfigNode::GetParent() const
{
    return this->parent;
}

void TConfigNode::SetIndexer(Indexer indexer)
{
    this->indexer = indexer;
}

bool TConfigNode::IsDictionary() const
{
    return this->indexer == Indexer::Dictionary;
}

bool TConfigNode::IsArray() const
{
    return this->indexer == Indexer::Array || this->indexer == Indexer::None;
}

bool TConfigNode::IsEmpty() const
{
    return this->GetType() == Type::Empty;
}

bool TConfigNode::Has(int index)
{
    return Has(TString::Itoa(index,10));
}

bool TConfigNode::Has(const char *tag) 
{
    auto it = std::find_if(elements.begin(), elements.end(), [&tag](TConfigNode* e) { return e != NULL && e->GetTag().EqualTo(tag); });
    return (it == elements.end());
}

TConfigNode &TConfigNode::Get(int index)
{
    Indexer bak = this->indexer;
    TConfigNode &node = Get(TString::Itoa(index,10));

    if(bak == Indexer::Array || bak == Indexer::None) {
        this->indexer = Indexer::Array;
    }

    return node;
}

TConfigNode &TConfigNode::Get(const char *tag) 
{
    auto it = std::find_if(elements.begin(), elements.end(), [&tag](TConfigNode* e) { return e != NULL && e->GetTag().EqualTo(tag); });
    if(it == elements.end()) {

        TConfigNode *node = new TConfigNode(tag);
        this->Attach(*node);

        it = elements.end()-1;
        this->SetIndexer(Indexer::Dictionary);
    }

    return **it;
}

std::vector<TConfigNode *> TConfigNode::GetElements() const
{
    return elements;
}

std::unordered_map<TString, TString> TConfigNode::GetAttributes() const 
{
    return attributes;
}

TString TConfigNode::GetPath() const
{
    if(this->parent == NULL) return this->GetTag();
    return this->parent->GetPath() + "." + this->GetTag();
}

TConfigNode &TConfigNode::operator[](int index) 
{
    return Get(index);
}

TConfigNode &TConfigNode::operator[](const char *element) 
{
    return Get(element);
}

void TConfigNode::Attach(TConfigNode& element, Insert position, const TConfigNode& specificElement)
{
    if (&element == this || IsAncestorOf(element)) {
        std::cerr << "Warning: cyclic loop `" << element.GetTag() << "` detected in the tree structure. skip." << std::endl;
        return;
    }

    auto it = std::find_if(elements.begin(), elements.end(), [&](TConfigNode* e) { return e == &specificElement; });
    if (it != elements.end()) {
        position == Insert::Before ? elements.insert(it, &element) : elements.insert(it + 1, &element);
    } else {
        elements.push_back(&element);
    }
    
    if (element.parent) {
        element.parent->Detach(element);
    }
    element.parent = this;
}

void TConfigNode::Detach(TConfigNode& element)
{
    auto it = std::find(elements.begin(), elements.end(), &element);
    if (it != elements.end()) {
        elements.erase(it); // Remove the element if found
        element.parent = nullptr; // Disconnect the removed element from the tree
    }
}

TConfigNode::Value TConfigNode::GetContent() const
{
    return this->content;
}

TString TConfigNode::AsString() const
{
    if (this->GetType() == Type::Integer) return Form("%d", std::get<int>(this->GetContent()));
    if (this->GetType() == Type::Float) return Form("%g", std::get<float>(this->GetContent()));
    if (this->GetType() == Type::Double) return Form("%f", std::get<double>(this->GetContent()));
    if (this->GetType() == Type::Boolean) return std::get<bool>(this->GetContent()) == 0 ? "false" : "true";
    if (this->GetType() == Type::Null) return "null";
    if (this->GetType() == Type::Empty) return "";

    return "";
}

TConfigNode::Type TConfigNode::GetType() const 
{
    if(std::get_if<std::nullptr_t>(&content))
        return Type::Null;
    if(std::get_if<bool>(&content))
        return Type::Boolean;
    if(std::get_if<int>(&content))
        return Type::Integer;
    if(std::get_if<float>(&content))
        return Type::Float;
    if(std::get_if<double>(&content))
        return Type::Double;
    if(std::get_if<TString>(&content))
        return Type::String;

    return Type::Empty;
}

void TConfigNode::SetContent(const Value &content)
{
    this->content = content;
}

void TConfigNode::SetTag(TString tag)
{
    this->tag = tag;
}

void TConfigNode::AddAttribute(const TString& key, const TString& value) {
    attributes[key] = value;
}

void TConfigNode::RemoveAttribute(const TString &key) {

    auto it = attributes.find(key);
    if (it != attributes.end()) {
        attributes.erase(it);
    }
}

TString TConfigNode::GetTag() const {
    return tag;
}

bool TConfigNode::IsSelfClosing() const {
    return elements.empty() && this->GetType() == Type::Empty;
}

bool TConfigNode::HasAttribute(const TString& key) const {
    return attributes.find(key) != attributes.end();
}

TString TConfigNode::GetAttribute(const TString& key) const {
    auto it = attributes.find(key);
    return it != attributes.end() ? it->second : TString();
}

TConfigNode* TConfigNode::Next()
{
    TConfigNode *next = this;
    std::function<void(const TString&, const TConfigNode&, const int)> _next_callback = [&next](const TString& tag, const TConfigNode& node, const int depth) mutable {

        UNUSED(tag);
        UNUSED(depth); 
        
        TConfigNode *_node = (TConfigNode *) &node; 
        if(next == _node) return;
        
        next = _node;
        throw std::exception();
    };

    try { this->PreOrderWalk(_next_callback); }
    catch(...) { return next; }

    do {

        if(next->parent == nullptr) return nullptr;
        std::vector<TConfigNode*> siblings = next->parent->elements;
        
        auto it = std::find(siblings.begin(), siblings.end(), next);
        if (it+1 != siblings.end()) return *(it+1);

        next = next->parent;

    } while(next != NULL);

    return nullptr;
}
        
void TConfigNode::Walk(TraversalOrder order, std::function<void(const TString&, const TConfigNode&, const int depth)> callback) const {

    switch (order) {
        case TraversalOrder::PreOrder:
            PreOrderWalk(callback);
            break;
        case TraversalOrder::InOrder:
            InOrderWalk(callback);
            break;
        case TraversalOrder::PostOrder:
            PostOrderWalk(callback);
            break;
    }
}

void TConfigNode::PreOrderWalk(std::function<void(const TString&, const TConfigNode&, const int depth)> callback) const {
    callback(tag, *this, this->GetDepth());
    for (const auto& element : elements) {
        element->PreOrderWalk(callback);
    }
}

void TConfigNode::InOrderWalk(std::function<void(const TString&, const TConfigNode&, const int depth)> callback) const {
    if (!elements.empty()) {
        elements.front()->InOrderWalk(callback);
    }
    callback(tag, *this, this->GetDepth());
    if (elements.size() > 1) {
        for (size_t i = 1; i < elements.size(); ++i) {
            elements[i]->InOrderWalk(callback);
        }
    }
}

int TConfigNode::GetDepth() const
{
    int depth = 0;
    
    const TConfigNode* currentNode = this;
    while (currentNode->parent != nullptr) {
        currentNode = currentNode->parent;
        depth++;
    }

    return depth;
}

bool TConfigNode::IsAncestorOf(TConfigNode& node) const {
    for (TConfigNode* current = node.parent; current != nullptr; current = current->parent) {
        if (current == this) {
            return true;
        }
    }
    return false;
}

void TConfigNode::PostOrderWalk(std::function<void(const TString&, const TConfigNode&, const int depth)> callback) const {
    for (const auto& element : elements) {
        element->PostOrderWalk(callback);
    }
    callback(tag, *this, this->GetDepth());
}

void TConfigNode::Print(Option_t *opt)
{
    int depth0 = this->GetDepth()-1;

    // Define a callback for Walk that prints the node's tag with indentation
    std::function<void(const TString&, const TConfigNode&, const int)> _print_callback = [depth0](const TString& tag, const TConfigNode& node, const int depth) mutable {
        std::cout << std::spacer(4*(depth-depth0), ' ') << "(TConfigNode *): " << &node << "; Tag = \"" << tag << "\"" << std::endl;
    };

    TString option = opt;
            option.ToLower();
    
    if(option.Contains("in")) return this->InOrderWalk(_print_callback);
    else if(option.Contains("post")) return this->PostOrderWalk(_print_callback);
    else if(option.Contains("pre")) return this->PreOrderWalk(_print_callback);

    _print_callback(this->GetTag(), *this, 0);
    for(int i = 0, N = this->elements.size(); i < N; i++) {
        if(elements[i] == nullptr) continue;
        _print_callback(elements[i]->GetTag(), *elements[i], 1);
    }
}

int TConfigNode::GetN() const
{
    return elements.size();
}

void TConfigNode::ClearContent()
{
    this->SetContent(std::monostate{});
}

void TConfigNode::Dump(Option_t *opt) const
{
    int depth0 = this->GetDepth();

    // Define a callback for Walk that prints the node's tag with indentation
    std::function<void(const TString&, const TConfigNode&, const int)> _dump_callback = [depth0](const TString& tag, const TConfigNode& node, const int depth) mutable {

            std::cout << std::spacer(4*(depth-depth0), ' ') << "(TConfigNode *): " << &node << "; Tag = \"" << tag << "\"";
            if (node.GetType() != Type::Empty) { // Adjust according to your TString implementation

                std::cout << ", Type: " << TConfigNode::enum2str(node.GetType());
                if (node.GetType() != Type::Null) 
                    std::cout << ", Content: \"" << node << "\"";
            }

            if (!node.attributes.empty()) {
                std::cout << ", Attributes: ";
                for (const auto& attr : node.attributes) {
                    std::cout << attr.first << "=" << attr.second << "; ";
                }
            }

            if(node.GetElements().size() > 0 && node.IsArray()) std::cout << " (array)";
            std::cout << std::endl;
        };

    TString option = opt;
            option.ToLower();

    if(option.Contains("in")) return this->InOrderWalk(_dump_callback);
    else if(option.Contains("post")) return this->PostOrderWalk(_dump_callback);
    else if(option.Contains("pre")) return this->PreOrderWalk(_dump_callback);
    else {

        _dump_callback(this->GetTag(), *this, 0);
        for(int i = 0, N = this->elements.size(); i < N; i++) {
            if(elements[i] == nullptr) continue;
            _dump_callback(elements[i]->GetTag(), *elements[i], 1);
        }
    }
}
