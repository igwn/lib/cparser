/**
 **********************************************
 *
 * \file TConfigParser.cc
 * \brief Source code of the TConfigParser class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "RCParser/TConfigParser.h"

#include "libRIOPlus.Config.h"
#ifdef Curl_FOUND
    #include <TCurl.h>
#endif
#ifdef Archive_FOUND
    #include <TArchive.h>
#endif

ClassImp(TConfigParser)

std::vector<std::unique_ptr<TConfigAdapter>> TConfigParser::adapters = {};

TConfigParser::TConfigParser(TString filename, Option_t *opt, int ttl, const char* adapterName): traversalOrder(TConfigNode::TraversalOrder::PreOrder)
{
    #ifdef Curl_FOUND
    if(!TCurl::IsLocal(filename)) {

        FILE *f = ROOT::IOPlus::Helpers::TempFileName("curl-", &fName);
        fclose(f);

        TCurl myFile((fName + TArchive::enum2str(TArchive::GetCompression(filename))), "", ttl, "cparser");
        if(!myFile.Fetch(filename, opt)) {
            std::cerr << "Failed to download: " << myFile.GetError() << " (use V option for more information)" << std::endl;
            return;
        }
        filename = fName;
    }
    #endif

    #ifdef Archive_FOUND
    TArchive archive(filename);
    if(archive.IsCompressed() && archive.GetN() == 1) {

        TString memberPath = archive.GetListOfMembers()[0];
        TString destination = ROOT::IOPlus::Helpers::TempDirectory(6) + memberPath;

        if(archive.ExtractMember(memberPath, destination)) {
            filename = destination;
        }
    }
    #endif

    filename = gSystem->ExpandPathName(filename.Data());
    fName = filename;

    TConfigParser::InitializeAdapters();
    if(!TString(adapterName).EqualTo(""))
        filename += "." + TString(adapterName);

    filename.ToLower();

    for(auto &adapter : TConfigParser::adapters) {

        if(!adapter->Supports(filename)) continue;

        TConfigNode *root = adapter->Load(fName);
        this->SetContent(root->GetContent());
        this->SetIndexer(root->GetIndexer());
        
        for(auto element : root->GetElements()) {
            this->Attach(*element);
        }
        for(auto attribute : root->GetAttributes()) {
            this->AddAttribute(attribute.first, attribute.second);
        }

        adapterName = adapter->GetName();
        return;
    }

}

TConfigParser::TConfigParser(TConfigNode *root, const char* adapterName): traversalOrder(TConfigNode::TraversalOrder::PreOrder)
{
    TConfigParser::InitializeAdapters();
    
    this->SetContent(root->GetContent());
    this->SetIndexer(root->GetIndexer());

    for(auto element : root->GetElements()) {
        this->Attach(*(new TConfigNode(*element)));
    }

    for(auto attribute : root->GetAttributes()) {
        this->AddAttribute(attribute.first, attribute.second);
    }

    this->adapterName = adapterName;
}


TConfigParser::~TConfigParser() { }

TConfigNode *TConfigParser::Parse(const char *contents, const char *adapterName)
{
    TConfigParser::InitializeAdapters();
    for(auto &adapter : TConfigParser::adapters) {

        if (strcmp(adapter->GetName(), adapterName) == 0) {
            return adapter->Parse(contents, TString(contents).Length());
        }
    }

    return nullptr;
}

void TConfigParser::Print(Option_t* opt)
{
    std::cout << "TConfigParser: " << fName << "; ";
    if(!adapterName.EqualTo("")) std::cout << "Adapter selected: `" << adapterName << "`" << std::endl;
    else std::cout << "Adapter auto-detection" << std::endl;
    
    TConfigNode::Print(opt);
}

void TConfigParser::Dump(Option_t* opt)
{
    std::cout << "TConfigParser: " << fName << "; ";
    if(!adapterName.EqualTo("")) std::cout << "Adapter selected: `" << adapterName << "`" << std::endl;
    else std::cout << "Adapter auto-detection" << std::endl;
    
    return TConfigNode::Dump(opt);
}

void TConfigParser::Write(const char *filename, const char *adapterName)
{
    TString _compression = TArchive::enum2str(TArchive::GetCompression(filename));
    TString filenameWithoutCompression = filename;
    if (filenameWithoutCompression.EndsWith("." + _compression))
        filenameWithoutCompression.Resize(filenameWithoutCompression.Length() - _compression.Length() - 1);

    TString _filename = filenameWithoutCompression;
    if(!TString(adapterName).EqualTo(""))
        _filename += "." + TString(adapterName);

    _filename.ToLower();

    TString fName = filename;
    for(auto &adapter : TConfigParser::adapters) {

        if(!adapter->Supports(_filename)) continue;
        
        if(!_compression.EqualTo("")) {
            FILE *f = ROOT::IOPlus::Helpers::TempFileName("config-", &fName);
            fclose(f);
        }

        adapter->Save(fName, *this);
        break;
    }

    #ifdef Archive_FOUND
    if(!_compression.EqualTo("")) {
        TArchive archive(filename, "RECREATE");
                 archive.Add(filenameWithoutCompression, fName);
                 archive.Write(filename);
    }
    #endif
}
