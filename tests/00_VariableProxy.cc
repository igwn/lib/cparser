#include "00_VariableProxy.h"

TEST(VariableProxy, INI) {
      
    #ifndef INI_FOUND
        GTEST_SKIP() << "Feature not enabled, skipping test.";
    #endif 

    std::cout << std::endl << "Making new configuration.." << std::endl;
    TConfigNode myNode = MakeConfig();
                myNode.DumpAll();

    std::cout << std::endl << "Writing into `test.ini`.." << std::endl;
    TConfigParser myWriter(&myNode);
                  myWriter.Write("test.ini");

    int ret = false;
    if(!TFileReader::Exists("test.ini")) std::cerr << "Failed to write `test.ini`.." << std::endl;
    else {

        myWriter.DumpAll();

        std::cout << std::endl << "Reading back `test.ini`.." << std::endl;
        TConfigParser myParser("test.ini");
                      myParser.DumpAll();

        ret = (myParser.GetElements().size() > 0) && (myWriter == myParser);
    }

    ASSERT_EQ(ret, true);
}

TEST(VariableProxy, JSON) {
      
    #ifndef JSON_FOUND
        GTEST_SKIP() << "Feature not enabled, skipping test.";
    #endif 

    std::cout << std::endl << "Making new configuration.." << std::endl;
    TConfigNode myNode = MakeConfig();
                myNode.DumpAll();

    std::cout << std::endl << "Writing into `test.json`.." << std::endl;
    TConfigParser myWriter(&myNode);
                  myWriter.Write("test.json");

    int ret = false;
    if(!TFileReader::Exists("test.json")) std::cerr << "Failed to write `test.json`.." << std::endl;
    else {

        myWriter.DumpAll();

        std::cout << std::endl << "Reading back `test.json`.." << std::endl;
        TConfigParser myParser("test.json");
                      myParser.DumpAll();

        ret = (myParser.GetElements().size() > 0) && (myWriter == myParser);
    }

    ASSERT_EQ(ret, true);
}

TEST(VariableProxy, TOML) {
      
    #ifndef TOML_FOUND
        GTEST_SKIP() << "Feature not enabled, skipping test.";
    #endif 

    std::cout << std::endl << "Making new configuration.." << std::endl;
    TConfigNode myNode = MakeConfig();
                myNode.DumpAll();

    std::cout << std::endl << "Writing into `test.toml`.." << std::endl;
    TConfigParser myWriter(&myNode);
                  myWriter.Write("test.toml");

    int ret = false;
    if(!TFileReader::Exists("test.toml")) std::cerr << "Failed to write `test.toml`.." << std::endl;
    else {

        myWriter.DumpAll();

        std::cout << std::endl << "Reading back `test.toml`.." << std::endl;
        TConfigParser myParser("test.toml");
                      myParser.DumpAll();

        ret = (myParser.GetElements().size() > 0) && (myWriter == myParser);
    }

    ASSERT_EQ(ret, true);
}

TEST(VariableProxy, XML) {
      
    #ifndef XML_FOUND
        GTEST_SKIP() << "Feature not enabled, skipping test.";
    #endif 

    std::cout << std::endl << "Making new configuration.." << std::endl;
    TConfigNode myNode = MakeConfig();
                myNode.DumpAll();

    std::cout << std::endl << "Writing into `test.xml`.." << std::endl;
    TConfigParser myWriter(&myNode);
                  myWriter.Write("test.xml");

    int ret = false;
    if(!TFileReader::Exists("test.xml")) std::cerr << "Failed to write `test.xml`.." << std::endl;
    else {

        myWriter.DumpAll();

        std::cout << std::endl << "Reading back `test.xml`.." << std::endl;
        TConfigParser myParser("test.xml");
                      myParser.DumpAll();

        ret = (myParser.GetElements().size() > 0) && (myWriter == myParser);
    }

    ASSERT_EQ(ret, false);
}

TEST(VariableProxy, YAML) {
      
    #ifndef YAML_FOUND
        GTEST_SKIP() << "Feature not enabled, skipping test.";
    #endif 

    std::cout << std::endl << "Making new configuration.." << std::endl;
    TConfigNode myNode = MakeConfig();
                myNode.DumpAll();
    
    std::cout << std::endl << "Writing into `test.yaml`.." << std::endl;
    TConfigParser myWriter(&myNode);
                  myWriter.Write("test.yaml");

    int ret = false;
    if(!TFileReader::Exists("test.yaml")) std::cerr << "Failed to write `test.yaml`.." << std::endl;
    else {

        myWriter.DumpAll();

        std::cout << std::endl << "Reading back `test.yaml`.." << std::endl;
        TConfigParser myParser("test.yaml");
                      myParser.DumpAll();

        ret = (myParser.GetElements().size() > 0) && (myWriter == myParser);
    }

    ASSERT_EQ(ret, true);
}

int main(int argc,char **argv) {

    // Initialize Google Test framework
    ::testing::InitGoogleTest(&argc,argv);

    // Run all tests
    return RUN_ALL_TESTS();
}
